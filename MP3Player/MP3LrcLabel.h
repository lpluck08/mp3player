//
//  MP3LrcLabel.h
//  MP3Player
//
//  Created by fangjiawang fangjiawang on 12-6-16.
//  Copyright (c) 2012年 fangjiawang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MP3LrcLabel : UILabel

// 此行歌词开始时间
@property (nonatomic, assign) float startTime;

// 设置原始的一行lrc
- (void) setLrcOfLine:(NSString *)lrc;

@end
