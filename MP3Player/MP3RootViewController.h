//
//  MP3RootViewController.h
//  MP3Player
//
//  Created by fangjiawang fangjiawang on 12-6-15.
//  Copyright (c) 2012年 fangjiawang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MP3.h"
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>

@interface MP3RootViewController : UIViewController <AVAudioPlayerDelegate, UIScrollViewDelegate>

// 复读开始时间按钮
@property (nonatomic, retain) IBOutlet UIButton *repeatStartTimeButton;

// 复读结束时间按钮
@property (nonatomic, retain) IBOutlet UIButton *repeatEndTimeButton;

// 取消复读跟读按钮
@property (nonatomic, retain) IBOutlet UIButton *cancelRepeatButton;

// 复读提示信息label
@property (nonatomic, retain) IBOutlet UILabel *repeatPointLabel;

// 复读开始时间label
@property (nonatomic, retain) IBOutlet UILabel *repeatStartLabel;

// 复读结束时间label
@property (nonatomic, retain) IBOutlet UILabel *repeatEndLabel;

// 跟读按钮
@property (nonatomic, retain) IBOutlet UIButton *followButton;

// 对比按钮
@property (nonatomic, retain) IBOutlet UIButton *compareButton;

// 歌词展示scroll view
@property (nonatomic, retain) IBOutlet UIScrollView *lrcScrollView;

// 拖动歌词提示线
@property  (nonatomic, retain) IBOutlet UIView *lrcPointLine;

// 拖动歌词提示时间label
@property  (nonatomic, retain) IBOutlet UILabel *lrcPointLabel;

// 进度条
@property (nonatomic, retain) IBOutlet UISlider *progressSlider;

// 时间进度label
@property (nonatomic, retain) IBOutlet UILabel *timeProgressLabel;

// 剩余时间label
@property (nonatomic, retain) IBOutlet UILabel *timeLaveLabel;

// 播放按钮
@property (nonatomic, retain) IBOutlet UIButton *playStopButton;

// 快退按钮
@property (nonatomic, retain) IBOutlet UIButton *rewindButton;

// 快进按钮
@property (nonatomic, retain) IBOutlet UIButton *fastForwardButton;

// 拖动播放进度条
- (IBAction) changeProgress:(id)sender;

// 点击播放/暂停按钮
- (IBAction) touchPlayStopButton:(id)sender;

// 快退
- (IBAction) touchRewindButton:(id)sender;

// 快进
- (IBAction) touchFastForwardButton:(id)sender;

// 点击记录复读开始时间按钮
- (IBAction) touchRepeatStartTimeButton:(id)sender;

// 点击记录复读结束时间按钮
- (IBAction) touchRepeatEndTimeButton:(id)sender;

// 取消复读
- (IBAction) touchCancelRepeatButton:(id)sender;

// 点击跟读按钮
- (IBAction) touchFollowButton:(id)sender;

// 点击对比按钮
- (IBAction) touchCompareButton:(id)sender;

@end
