//
//  MP3.h
//  MP3Player
//
//  Created by fangjiawang fangjiawang on 12-6-15.
//  Copyright (c) 2012年 fangjiawang. All rights reserved.
//

#ifndef MP3Player_MP3_h
#define MP3Player_MP3_h

/**
 * 自定义log，目的:在release之后，遗留的输出log函数不会有输出
 */
#import "HJLog.h"
#if defined (DEBUG) && DEBUG == 1
#define MP3Log(...) HJLog(__VA_ARGS__)
#else
#define MP3Log(...) do {} while (0)
#endif

#endif
