//
//  MP3LrcLabel.m
//  MP3Player
//
//  Created by fangjiawang fangjiawang on 12-6-16.
//  Copyright (c) 2012年 fangjiawang. All rights reserved.
//

#import "MP3LrcLabel.h"

@implementation MP3LrcLabel

@synthesize startTime;

#pragma mark - 内存管理

- (void) dealloc {
    
    [super dealloc];
}

- (id) initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.numberOfLines = 1;
    }
    return self;
}

#pragma mark - 实现接口方法

// 设置原始的一行lrc
- (void) setLrcOfLine:(NSString *)lrc {
    lrc = @"[00:06.01]我的未来不是梦";
}

@end
