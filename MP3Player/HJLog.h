//
//  HJLog.h
//  MP3Player
//
//  Created by fangjiawang fangjiawang on 12-6-15.
//  Copyright (c) 2012年 fangjiawang. All rights reserved.
//  作用：格式化log输出：文件:行号 方法名; 真正的输出

#import <Foundation/Foundation.h>

@interface HJLog : NSObject

+ (void) file:(char*)sourceFile function:(char*)functionName lineNumber:(int)lineNumber format:(NSString*)format, ...;

#define HJLog(s, ...) [HJLog file:__FILE__ function: (char *)__FUNCTION__ lineNumber:__LINE__ format:(s), ##__VA_ARGS__]

@end
