//
//  main.m
//  MP3Player
//
//  Created by fangjiawang fangjiawang on 12-6-15.
//  Copyright (c) 2012年 fangjiawang. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MP3AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MP3AppDelegate class]));
    }
}
