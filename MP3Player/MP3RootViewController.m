//
//  MP3RootViewController.m
//  MP3Player
//
//  Created by fangjiawang fangjiawang on 12-6-15.
//  Copyright (c) 2012年 fangjiawang. All rights reserved.
//

#import "MP3RootViewController.h"
#import "RegexKitLite.h"
#import "MP3LrcLabel.h"

@interface MP3RootViewController ()

// 音乐播放器
@property (nonatomic, retain) AVAudioPlayer *mp3Player;

// 存放录音的文件
@property (nonatomic, retain) NSURL *soundFile;

// 录音器
@property (nonatomic, retain) AVAudioRecorder *soundRecorder;

// 录音播放器
@property (nonatomic, retain) AVAudioPlayer *soundAudioPlayer;

// 作业调度
@property (nonatomic, retain) NSTimer *scheduledTimer;

// 复读调度
@property (nonatomic, retain) NSTimer *repeatScheduledTimer;

// 是否正在跟读
@property (nonatomic) BOOL following;

// 拖动后的时间
@property (nonatomic) float endDragTime;

// 复读开始时间
@property (nonatomic) float repeatStartTime;

// 复读结束时间
@property (nonatomic) float repeatEndTime;

// 加载歌词
- (void) loadLRC;

// 更新进度
- (void) updateProgress;

// 更新歌词显示进度
- (void) updateLrcProgress;

// 暂停后连带事件
- (void) afterStop;

// 开始后连带事件
- (void) afterStart;

// 格式化秒数为:00:00
- (NSString *) formatSeconds:(float)seconds;

// 显示和隐藏复读控件view
- (void) hideRepeatView:(BOOL)hide;

// 更新复读状态
- (void) updateRepeat;

// 开始复读
- (void) playRepeat;

// 更新对比状态
- (void) updateCompare;

@end

// 快进/快退跳跃秒数
static float JumpInterval = 10.;

// 歌曲名称
static NSString *SongName = @"胡彦斌-我的未来不是梦";

@implementation MP3RootViewController

@synthesize mp3Player, soundFile, soundRecorder, soundAudioPlayer;

@synthesize scheduledTimer, repeatScheduledTimer;

@synthesize following, endDragTime;

@synthesize repeatStartTime, repeatEndTime;

@synthesize repeatStartTimeButton, repeatEndTimeButton, cancelRepeatButton, repeatPointLabel, repeatStartLabel, repeatEndLabel, followButton, compareButton, lrcScrollView, lrcPointLine, lrcPointLabel, progressSlider, timeLaveLabel, timeProgressLabel, playStopButton, rewindButton, fastForwardButton;

#pragma mark - 内存管理

- (void) dealloc {
    [self.mp3Player release];
    [self.soundFile release];
    [self.soundRecorder release];
    [self.soundAudioPlayer release];
    [self.scheduledTimer release];
    [self.repeatScheduledTimer release];
    
    [self.repeatStartTimeButton release];
    [self.repeatEndTimeButton release];
    [self.cancelRepeatButton release];
    [self.repeatPointLabel release];
    [self.repeatStartLabel release];
    [self.repeatEndLabel release];
    [self.followButton release];
    [self.compareButton release];
    [self.lrcScrollView release];
    [self.lrcPointLine release];
    [self.lrcPointLabel release];
    [self.progressSlider release];
    [self.timeLaveLabel release];
    [self.timeProgressLabel release];
    [self.playStopButton release];
    [self.rewindButton release];
    [self.fastForwardButton release];
    
    [super dealloc];
}

- (id) initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // 初始化是否正在跟读状态为NO
        following = NO;
        /**
         * 设置支持后台播放
         * 参照：http://developer.apple.com/library/ios/#qa/qa1668/_index.html
         */
        {
            AVAudioSession *audioSession = [AVAudioSession sharedInstance];
            
            NSError *setCategoryError = nil;
            // 设置类别为播放和录音
            BOOL success = [audioSession setCategory:AVAudioSessionCategoryPlayAndRecord 
                                               error:&setCategoryError];
            if (!success) {
                /* 处理异常 */
            }
            
            NSError *activationError = nil;
            success = [audioSession setActive:YES error:&activationError];
            if (!success) {
                /* 处理异常 */
            }
        }
        
        NSError *error = nil;
        
        // 声明录音器
        NSString *tempDir = NSTemporaryDirectory();
        self.soundFile = [NSURL fileURLWithPath:[tempDir stringByAppendingString:@"sound.caf"]];
        NSDictionary *soundSetting = [NSDictionary dictionaryWithObjectsAndKeys:
                        [NSNumber numberWithFloat:44100.0],AVSampleRateKey,
                        [NSNumber numberWithInt:kAudioFormatMPEG4AAC],AVFormatIDKey,
                        [NSNumber numberWithInt:2],AVNumberOfChannelsKey,
                        [NSNumber numberWithInt:AVAudioQualityHigh],AVEncoderAudioQualityKey,
                        nil];
        soundRecorder = [[AVAudioRecorder alloc] initWithURL:self.soundFile
                                                    settings:soundSetting
                                                       error:&error];
        if (error) {
            /* 处理异常 */
            MP3Log(@"%@", error);
            error = nil;
        }
        [self.soundRecorder prepareToRecord];
        
        // 声明音乐播放器
        NSURL *movieFile = [NSURL fileURLWithPath:
                            [[NSBundle mainBundle] pathForResource:SongName 
                                                            ofType:@"mp3"]];
        
        mp3Player = [[AVAudioPlayer alloc] initWithContentsOfURL:movieFile
                                                           error:&error];
        
        if (error) {
            /* 处理异常 */
            MP3Log(@"%@", error);
        }
        [self.mp3Player setDelegate:self];
        [self.mp3Player setNumberOfLoops:0];
        [self.mp3Player prepareToPlay];
    }
    return self;
}

- (void) viewDidLoad {
    [super viewDidLoad];
    
    // 隐藏复读控件view
    [self hideRepeatView:YES];
    // 隐藏跟读控件view
    [self.followButton setHidden:YES];
    [self.compareButton setHidden:YES];
    
    [self.progressSlider setMinimumValue:.0];
    [self.progressSlider setMaximumValue:self.mp3Player.duration];
    [self.timeLaveLabel setText:[self formatSeconds:self.mp3Player.duration]];
    [self.lrcScrollView setDelegate:self];
    [self.lrcScrollView setContentSize:self.lrcScrollView.frame.size];
    // 设置歌词的拖动速率为0
    [self.lrcScrollView setDecelerationRate:.0];
    
    // 隐藏拖动歌词提示信息
    [self.lrcPointLabel setHidden:YES];
    [self.lrcPointLine setHidden:YES];
    
    self.repeatScheduledTimer = nil;
    
    [self loadLRC];
}

- (void) viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - AVAudioPlayerDelegate

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag {
    if (player == self.mp3Player) {
        [self.progressSlider setValue:.0];
        [self afterStop];
    } else if (player == self.soundAudioPlayer) {
        [self performSelector:@selector(playRepeat) withObject:nil afterDelay:.5];
    }
}

#pragma mark - UIScrollViewDelegate

- (void) scrollViewDidScroll:(UIScrollView *)scrollView {
    // 中间所在content offset的y
    CGFloat middleY = scrollView.contentOffset.y + scrollView.frame.size.height / 2;
    
    for (UIView *_v in scrollView.subviews) {
        if ([_v isKindOfClass:[MP3LrcLabel class]]) {
            MP3LrcLabel *lrcLabel = (MP3LrcLabel *)_v;
            [lrcLabel setTextColor:[UIColor blackColor]];
            if (middleY >= lrcLabel.frame.origin.y && middleY < lrcLabel.frame.origin.y + lrcLabel.frame.size.height) {
                [lrcLabel setTextColor:[UIColor blueColor]];
                [self.lrcPointLabel setText:[self formatSeconds:lrcLabel.startTime]];
                self.endDragTime = lrcLabel.startTime;
            }
        }
    }
}

- (void) scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    // 显示拖动歌词提示信息
    [self.lrcPointLabel setHidden:NO];
    [self.lrcPointLine setHidden:NO];
}

- (void) scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    // 隐藏拖动歌词提示信息
    [self.lrcPointLabel setHidden:YES];
    [self.lrcPointLine setHidden:YES];
    [self.mp3Player setCurrentTime:self.endDragTime];
}

- (void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    // 隐藏拖动歌词提示信息
    [self.lrcPointLabel setHidden:YES];
    [self.lrcPointLine setHidden:YES];
    [self.mp3Player setCurrentTime:self.endDragTime];
}

#pragma mark - 私有方法

// 加载歌词
- (void) loadLRC {
    NSString *string = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:SongName ofType:@"lrc"] encoding:NSUTF8StringEncoding error:NULL];
    NSArray *lines = [string componentsSeparatedByString:@"\n"];
    CGFloat _y = self.lrcScrollView.contentSize.height / 2;
    for (NSString *originalLrc in lines) {
        // 使用了Regex做正则匹配
        if (originalLrc.length >= 10 && [originalLrc isMatchedByRegex:@"\\d\\d:\\d\\d.\\d\\d" inRange:NSMakeRange(0, 10)]) {
            // 歌词开始时间
            NSString *startTimeString = [originalLrc substringToIndex:10];
            // 开始分
            NSString *startTimeMinuteString = [startTimeString substringWithRange:NSMakeRange(1, 2)];
            // 开始秒
            NSString *startTimeSecondsString = [startTimeString substringWithRange:NSMakeRange(4, 5)];
            // 开始时间
            float startTime = [startTimeMinuteString intValue] * 60 + [startTimeSecondsString floatValue];
            
            // 歌词
            NSString *lrc = [originalLrc substringFromIndex:10];
            
            MP3LrcLabel *lrcLabel = [[MP3LrcLabel alloc] initWithFrame:CGRectMake(0, _y, self.lrcScrollView.frame.size.width, 30)];
            [lrcLabel setBackgroundColor:[UIColor clearColor]];
            [lrcLabel setStartTime:startTime];
            [lrcLabel setTextAlignment:UITextAlignmentCenter];
            [lrcLabel setTextColor:[UIColor blackColor]];
            [lrcLabel setFont:[UIFont systemFontOfSize:14.]];
            [lrcLabel setText:lrc];
            [self.lrcScrollView addSubview:lrcLabel];
            [lrcLabel release];
            
            [self.lrcScrollView setContentSize:CGSizeMake(self.lrcScrollView.contentSize.width, self.lrcScrollView.contentSize.height + lrcLabel.frame.size.height)];
            
            _y += lrcLabel.frame.size.height;
        }
    }
}

// 更新进度
- (void) updateProgress {
    [self.progressSlider setValue:self.mp3Player.currentTime];
    [self.timeProgressLabel setText:[self formatSeconds:self.mp3Player.currentTime]];
    [self updateLrcProgress];
}

// 更新歌词显示进度
- (void) updateLrcProgress {
    for (int i = 0; i < self.lrcScrollView.subviews.count; i ++) {
        UIView *thisView = [self.lrcScrollView.subviews objectAtIndex:i];
        UIView *nextView = nil;
        if (i + 1 < self.lrcScrollView.subviews.count) {
            nextView = [self.lrcScrollView.subviews objectAtIndex:i + 1];
        }
        if ([thisView isKindOfClass:[MP3LrcLabel class]]) {
            MP3LrcLabel *thisLrcLabel = (MP3LrcLabel *)thisView;
            MP3LrcLabel *nextLrcLabel = (MP3LrcLabel *)nextView;
            [thisLrcLabel setTextColor:[UIColor blackColor]];
            if (nextLrcLabel) {
                if (thisLrcLabel.startTime - 1 < self.mp3Player.currentTime && self.mp3Player.currentTime <= nextLrcLabel.startTime) {
                    [self.lrcScrollView setContentOffset:CGPointMake(0, thisLrcLabel.frame.origin.y - self.lrcScrollView.frame.size.height / 2)];
                    [thisLrcLabel setTextColor:[UIColor blueColor]];
                    break;
                }
            } else {
                if (thisLrcLabel.startTime - 1 < self.mp3Player.currentTime && self.mp3Player.currentTime <= self.mp3Player.duration) {
                    [thisLrcLabel setTextColor:[UIColor blueColor]];
                }
            }
        }
    }
}

// 暂停后连带事件
- (void) afterStop {
    [self.playStopButton setTitle:@"开始" forState:UIControlStateNormal];
    [self.rewindButton setEnabled:NO];
    [self.fastForwardButton setEnabled:NO];
    
    // 暂停调度
    [self.scheduledTimer invalidate];
    self.scheduledTimer = nil;
}

// 开始后连带事件
- (void) afterStart {
    [self.playStopButton setTitle:@"暂停" forState:UIControlStateNormal];
    [self.rewindButton setEnabled:YES];
    [self.fastForwardButton setEnabled:YES];
    
    // 开启作业调度
    self.scheduledTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                           target:self
                                                         selector:@selector(updateProgress)
                                                         userInfo:nil
                                                          repeats:YES];
}

// 格式化秒数为:00:00
- (NSString *) formatSeconds:(float)s {
    NSString *ret = @"";
    // 格式化分
    int minutes = (int)s / 60;
    NSString *minutesString = [NSString stringWithFormat:@"%d", minutes];
    if (minutesString.length == 1) {
        ret = [ret stringByAppendingFormat:@"0%d", minutes];
    } else {
        ret = [ret stringByAppendingFormat:@"%d", minutes];
    }
    
    ret = [ret stringByAppendingFormat:@":"];
    
    // 格式化秒
    int seconds = (int)s % 60;
    NSString *secondsString = [NSString stringWithFormat:@"%d", seconds];
    if (secondsString.length == 1) {
        ret = [ret stringByAppendingFormat:@"0%d", seconds];
    } else {
        ret = [ret stringByAppendingFormat:@"%d", seconds];
    }
    return ret;
}

// 显示和隐藏复读控件view
- (void) hideRepeatView:(BOOL)hide {
    [self.repeatStartTimeButton setHidden:hide];
    [self.repeatEndTimeButton setHidden:hide];
    [self.cancelRepeatButton setHidden:hide];
    [self.repeatPointLabel setHidden:hide];
    [self.repeatStartLabel setHidden:hide];
    [self.repeatEndLabel setHidden:hide];
}

// 更新复读状态
- (void) updateRepeat {
    if (self.mp3Player.currentTime >= self.repeatEndTime) {
        [self.mp3Player setCurrentTime:self.repeatStartTime];
        
        [self.mp3Player pause];
        // 复读间隔.5秒
        [self performSelector:@selector(playRepeat) withObject:nil afterDelay:.5];
    }
}

// 开始复读
- (void) playRepeat {
    if (!following) {
        [self.mp3Player play];
    }
}

// 更新对比状态
- (void) updateCompare {
    if (!self.mp3Player.playing && !self.soundAudioPlayer.playing) {
        [self.mp3Player play];
    }
    if (self.mp3Player.currentTime >= self.repeatEndTime) {
        [self.mp3Player setCurrentTime:self.repeatStartTime];
        
        [self.mp3Player pause];
        [self.soundAudioPlayer play];
    }
}

#pragma mark - 实现接口方法

// 拖动播放进度条
- (IBAction) changeProgress:(id)sender {
    [self.mp3Player setCurrentTime:self.progressSlider.value];
    [self.timeProgressLabel setText:[self formatSeconds:self.progressSlider.value]];
}

// 点击播放/暂停按钮
- (IBAction) touchPlayStopButton:(id)sender {
    if (self.mp3Player.playing) {
        // 点击播放
        [self.mp3Player pause];
        [self afterStop];
        
        // 隐藏复读控件view
        [self hideRepeatView:YES];
        [self.repeatPointLabel setText:@"您可以设置复读开始时间"];
    } else {
        // 点击暂停
        [self.mp3Player play];
        [self afterStart];
        
        // 显示复读控件view
        [self hideRepeatView:NO];
    }
}

// 快退
- (IBAction) touchRewindButton:(id)sender {
    [self.mp3Player setCurrentTime:self.mp3Player.currentTime - JumpInterval];
    [self updateProgress];
}

// 快进
- (IBAction) touchFastForwardButton:(id)sender {
    [self.mp3Player setCurrentTime:self.mp3Player.currentTime + JumpInterval];
    [self updateProgress];
}

// 点击记录复读开始时间按钮
- (IBAction) touchRepeatStartTimeButton:(id)sender {
    if (self.mp3Player.playing) {
        self.repeatStartTime = self.mp3Player.currentTime;
        [self.repeatStartLabel setText:[@"复读开始时间: " stringByAppendingFormat:@"%@", 
                                        [self formatSeconds:self.repeatStartTime]]];
        [self.repeatPointLabel setText:@"请设置复读结束时间"];
    }
}

// 点击记录复读结束时间按钮
- (IBAction) touchRepeatEndTimeButton:(id)sender {
    if (self.mp3Player.playing && 0 != self.repeatStartTime) {
        self.repeatEndTime = self.mp3Player.currentTime;
        [self.repeatEndLabel setText:[@"复读结束时间: " stringByAppendingFormat:@"%@", 
                                      [self formatSeconds:self.repeatEndTime]]];
        
        [self.mp3Player setCurrentTime:self.repeatStartTime];
        
        [self.repeatPointLabel setText:@"您可以选择跟读"];
        
        // 显示跟读按钮
        [self.followButton setHidden:NO];
        
        // 开启复读调度
        if (self.repeatScheduledTimer) {
            [self.repeatScheduledTimer invalidate];
            self.repeatScheduledTimer = nil;
        }
        self.repeatScheduledTimer = [NSTimer scheduledTimerWithTimeInterval:.5
                                                               target:self
                                                             selector:@selector(updateRepeat)
                                                             userInfo:nil
                                                              repeats:YES];
    }
}

// 取消复读
- (IBAction) touchCancelRepeatButton:(id)sender {
    following = NO;
    [self.repeatScheduledTimer invalidate];
    self.repeatScheduledTimer = nil;
    [self.soundAudioPlayer stop];
    [self.soundRecorder stop];
    [self.soundRecorder stop];
    [self.followButton setTitle:@"跟读" forState:UIControlStateNormal];
    
    [self.repeatStartLabel setText:@"复读开始时间: ?"];
    [self.repeatEndLabel setText:@"复读结束时间: ?"];
    [self.repeatPointLabel setText:@"您可以设置复读开始时间"];
    
    // 隐藏跟读控件view
    [self.followButton setHidden:YES];
    [self.compareButton setHidden:YES];
    
    [self.mp3Player play];
}

// 点击跟读按钮
- (IBAction) touchFollowButton:(id)sender {
    following = YES;
    if (self.soundRecorder.recording) {
        // 暂停录音
        [self.soundRecorder stop];
        [self.followButton setTitle:@"跟读" forState:UIControlStateNormal];
        [self.compareButton setHidden:NO];
        [self.repeatPointLabel setText:@"您可以点击对比按钮，进行对比发音"];
    } else {
        // 开始录音
        [self.soundRecorder record];
        [self.followButton setTitle:@"停止" forState:UIControlStateNormal];
        [self.repeatPointLabel setText:@"请您跟读，正在录音..."];
    }
}

// 点击对比按钮
- (IBAction) touchCompareButton:(id)sender {
    [self.repeatPointLabel setText:@"正在对比..."];
    NSError *error = nil;
    // 声明录音播放器
    AVAudioPlayer *tmpAudioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:self.soundFile error:&error];
    if (error) {
        /* 处理异常 */
        MP3Log(@"%@", error);
    }
    self.soundAudioPlayer = tmpAudioPlayer;
    [tmpAudioPlayer release];
    
    [self.soundAudioPlayer setDelegate:self];
    [self.soundAudioPlayer setNumberOfLoops:0];
    [self.soundAudioPlayer prepareToPlay];
    
    [self.mp3Player setCurrentTime:self.repeatStartTime];
    // 开启复读调度
    if (self.repeatScheduledTimer) {
        [self.repeatScheduledTimer invalidate];
        self.repeatScheduledTimer = nil;
    }
    self.repeatScheduledTimer = [NSTimer scheduledTimerWithTimeInterval:.5
                                                                 target:self
                                                               selector:@selector(updateCompare)
                                                               userInfo:nil
                                                                repeats:YES];
    
}

@end
