//
//  HJLog.m
//  MP3Player
//
//  Created by fangjiawang fangjiawang on 12-6-15.
//  Copyright (c) 2012年 fangjiawang. All rights reserved.
//

#import "HJLog.h"

@implementation HJLog

+ (void) file:(char*)sourceFile function:(char*)functionName lineNumber:(int)lineNumber format:(NSString*)format, ... {
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	va_list ap;
	NSString *print, *file, *function;
	va_start(ap, format);
	file = [[NSString alloc] initWithBytes:sourceFile length:strlen(sourceFile) encoding:NSUTF8StringEncoding];
	
	function = [NSString stringWithCString:functionName encoding:NSUTF8StringEncoding];
	print = [[NSString alloc] initWithFormat:format arguments:ap];
	va_end(ap);
	NSLog(@"%@:%d %@; %@", [file lastPathComponent], lineNumber, function, print);
	[print release];
	[file release];
	[pool release];
}

@end
